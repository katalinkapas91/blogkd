package de.academy.blogkd.beitrag;

import de.academy.blogkd.kommentar.Kommentar;
import de.academy.blogkd.user.User;

import javax.persistence.*;
import java.time.Instant;
import java.util.List;

@Entity
public class Beitrag {

    @Id
    @GeneratedValue
    private long id;

    @ManyToOne
    private User user;

    @OneToMany(mappedBy = "beitrag", cascade = CascadeType.ALL)
    private List<Kommentar> kommentare;

    @Lob
    private String text;
    private String title;

    private Instant postedAt;

    public Beitrag() {
    }

    public Beitrag(User user, String text, Instant postedAt, String title) {
        this.user = user;
        this.text = text;
        this.postedAt = postedAt;
        this.title = title;
    }

    public long getId() {
        return id;
    }

    public User getUser() {
        return user;
    }

    public String getText() {
        return text;
    }

    public Instant getPostedAt() {
        return postedAt;
    }

    public List<Kommentar> getKommentare() {
        return kommentare;
    }

    public void setKommentare(List<Kommentar> kommentare) {
        this.kommentare = kommentare;
    }

    public String getTitle() {
        return title;
    }

    public void setText(String text) {
        this.text = text;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public void setPostedAt(Instant postedAt) {
        this.postedAt = postedAt;
    }
}
