package de.academy.blogkd.beitrag;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface BeitragRepository extends CrudRepository<Beitrag, Long> {

    List<Beitrag> findAllByOrderByPostedAtDesc();

    Optional<Beitrag> findBeitragById(long id);


}
