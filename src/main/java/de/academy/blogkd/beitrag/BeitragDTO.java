package de.academy.blogkd.beitrag;

import javax.persistence.Lob;
import javax.validation.constraints.Size;


public class BeitragDTO {

    @Size(min = 1, max = 500)
    @Lob
    private String text;

    @Size(min = 1, max = 99)
    private String title;

    public BeitragDTO(String text) {
        this.text = text;
    }

    public String getText() {
        return text;
    }

    public String getTitle() {
        return title;
    }

    public void setText(String text) {
        this.text = text;
    }

    public void setTitle(String title) {
        this.title = title;
    }
}
