package de.academy.blogkd.beitrag;

import de.academy.blogkd.kommentar.Kommentar;
import de.academy.blogkd.kommentar.KommentarDTO;
import de.academy.blogkd.kommentar.KommentarRepository;
import de.academy.blogkd.user.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.util.UriComponentsBuilder;

import javax.validation.Valid;
import java.time.Instant;

@Controller
public class BeitragController {

    private BeitragRepository beitragRepository;
    private KommentarRepository kommentarRepository;


    @Autowired
    public BeitragController(BeitragRepository beitragRepository, KommentarRepository kommentarRepository) {
        this.beitragRepository = beitragRepository;
        this.kommentarRepository = kommentarRepository;
    }

    @GetMapping("/beitrag")
    public String beitrag(Model model, @ModelAttribute("sessionUser") User sessionUser) {

        if (sessionUser != null) {
            model.addAttribute("beitrag", new BeitragDTO(""));
            return "beitrag";
        }
        return "redirect:/";
    }

    @PostMapping("/save")
    public String saveBeitrag(@Valid @ModelAttribute("beitrag") BeitragDTO beitragDTO, BindingResult bindingResult, @ModelAttribute("sessionUser") User sessionUser) {

        if (sessionUser.isAdmin()) {
            if (bindingResult.hasErrors()) {
                return "beitrag";
            }


            Beitrag beitrag = new Beitrag(sessionUser, beitragDTO.getText(), Instant.now(), beitragDTO.getTitle());
            beitragRepository.save(beitrag);

            return "redirect:/";
        }

        return "redirect:/";
    }

    @GetMapping("/edit/{id}")
    public String editBeitrag(@PathVariable long id, Model model) {
        Beitrag beitrag = beitragRepository.findById(id)
                .orElseThrow(() -> new IllegalArgumentException("Invalid user Id:" + id));
        model.addAttribute("beitrag", beitrag);
        model.addAttribute("kommentar", new KommentarDTO(""));
        model.addAttribute("kommentario", new Kommentar());


        return "beitragdetails";
    }

    @PostMapping("/edit/{id}/editedbeitrag")
    public String editedBeitrag(@PathVariable long id, BeitragDTO beitragDTO, BindingResult bindingResult, @ModelAttribute("sessionUser") User sessionUser) {
        if (bindingResult.hasErrors()) {
            return "beitrag";
        }

        if (sessionUser.isAdmin()) {
            Beitrag beitrag = beitragRepository.findById(id).orElseThrow(() -> new IllegalArgumentException("Invalid user Id:" + id));
            beitrag.setText(beitragDTO.getText());
            beitrag.setTitle(beitragDTO.getTitle());
            beitragRepository.save(beitrag);
            return "redirect:/";
        }

        return "redirect:/";

    }

    @PostMapping("/edit/{id}/savekommentar")
    public String saveKommentar(@PathVariable long id, @Valid @ModelAttribute KommentarDTO kommentarDTO, BindingResult bindingResult, @ModelAttribute("sessionUser") User sessionUser) {

        if (bindingResult.hasErrors()) {
            return "kommentar";
        }

        if (sessionUser != null) {
            Kommentar kommentar = new Kommentar(kommentarDTO.getText(), Instant.now(), beitragRepository.findBeitragById(id).get(), sessionUser);
            kommentarRepository.save(kommentar);

            return "redirect:/edit/{id}";
        }
        return "redirect:/edit/{id}";

    }

    @PostMapping("/beitragDelete")
    public String delete(@RequestParam long beitragId, @ModelAttribute("sessionUser") User sessionUser) {
        Beitrag beitrag = beitragRepository.findById(beitragId).orElseThrow();

//        if (beitrag.getUser() != sessionUser) {
//            throw new IllegalArgumentException("nein!");
//        }

        if (sessionUser.isAdmin()) {
            beitragRepository.delete(beitrag);
            kommentarRepository.deleteKommentarByBeitrag_Id(beitragId);
            return "redirect:" + UriComponentsBuilder.fromPath("/profile").pathSegment(sessionUser.getUsername()).toUriString();
        }

        return "redirect:" + UriComponentsBuilder.fromPath("/profile").pathSegment(sessionUser.getUsername()).toUriString();

    }

    @PostMapping("/edit/{id}/deletecomment")
    public String commentDelete(@PathVariable long id, @ModelAttribute("sessionUser") User sessionUser) {
        Kommentar kommentar = kommentarRepository.findById(id).orElseThrow();

        if (sessionUser.isAdmin() || sessionUser.getId() == kommentar.getUser().getId()) {
            kommentarRepository.delete(kommentar);
            return "redirect:/";
        }

        return "redirect:/";
    }
}

