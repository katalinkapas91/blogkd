package de.academy.blogkd;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BlogkdApplication {

    public static void main(String[] args) {
        SpringApplication.run(BlogkdApplication.class, args);
    }

}
