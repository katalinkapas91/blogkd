package de.academy.blogkd.profile;

import de.academy.blogkd.user.User;
import de.academy.blogkd.user.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;

@Controller
public class ProfileController {

    private UserRepository userRepository;

    @Autowired
    public ProfileController(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @GetMapping("/profile/{username}")
    public String profile(@PathVariable String username, Model model) {

        User user = userRepository.findByUsername(username).orElseThrow();
        model.addAttribute("user", user);
        return "profile";
    }


    @PostMapping("/profile/{username}/admin")
    public String admin(@PathVariable String username, @ModelAttribute("sessionUser") User sessionUser) {

        User user = userRepository.findByUsername(username).orElseThrow();

        if (sessionUser.isAdmin()) {
            user.setAdmin(true);

            userRepository.save(user);

            return "redirect:/";
        }
        return "redirect:/";
    }
}
