package de.academy.blogkd.user;

import de.academy.blogkd.beitrag.Beitrag;

import javax.persistence.*;
import java.util.List;

@Entity
public class User {

    @Id
    @GeneratedValue
    private long id;

    private String username;
    private String password;
    private boolean isAdmin = false;


    @OneToMany(mappedBy = "user", cascade = CascadeType.ALL)
    private List<Beitrag> beitraege;

    public User() {
    }

    public User(String username, String password) {
        this.username = username;
        this.password = password;

    }

    public String getUsername() {
        return username;
    }

    public List<Beitrag> getBeitraege() {
        return beitraege;
    }

    public long getId() {
        return id;
    }

    public boolean isAdmin() {
        return isAdmin;
    }

    public void setAdmin(boolean admin) {
        isAdmin = admin;
    }
}
