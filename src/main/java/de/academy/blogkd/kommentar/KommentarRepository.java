package de.academy.blogkd.kommentar;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface KommentarRepository extends CrudRepository<Kommentar, Long> {
    List<Kommentar> findAllByOrderByPostedAtAsc();

    List<Kommentar> deleteKommentarByBeitrag_Id(long id);

}
