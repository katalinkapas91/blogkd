package de.academy.blogkd.kommentar;

import javax.validation.constraints.Size;

public class KommentarDTO {

    @Size(min = 1, max = 99, message = "Min. 1 character is required")
    private String text;

    public KommentarDTO(String text) {
        this.text = text;
    }


    public String getText() {
        return text;
    }


}
