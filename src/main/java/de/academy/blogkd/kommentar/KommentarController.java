package de.academy.blogkd.kommentar;


import de.academy.blogkd.beitrag.BeitragRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;

import javax.validation.Valid;
import java.time.Instant;


@Controller
public class KommentarController {

    private KommentarRepository kommentarRepository;
    private BeitragRepository beitragRepository;


    @Autowired
    public KommentarController(KommentarRepository kommentarRepository, BeitragRepository beitragRepository) {
        this.kommentarRepository = kommentarRepository;
        this.beitragRepository = beitragRepository;
    }


    @GetMapping("/kommentar")
    public String kommentar(Model model) {
        model.addAttribute("kommentar", new KommentarDTO(""));
        return "kommentar";
    }
    

    @PostMapping("/kommentar")
    public String kommentar(@Valid @ModelAttribute("kommentar") KommentarDTO kommentarDTO, BindingResult bindingResult) {
        if (bindingResult.hasErrors()) {
            return "kommentar";
        }

        Kommentar kommentar = new Kommentar(kommentarDTO.getText(), Instant.now());

        kommentarRepository.save(kommentar);

        return "redirect:/";
    }


}
